import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from 'nativescript-ui-sidedrawer';
import { RouterExtensions } from 'nativescript-angular/router';
import { filter } from 'rxjs/operators';
import * as app from 'tns-core-modules/application';
import { AppService } from '@src/app/app.service';
import { IMenu } from '@src/app/IMenu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})

export class AppComponent {
  private _activatedUrl: string;
  private _sideDrawerTransition: DrawerTransitionBase;
  fillerNav: IMenu[];

  constructor(private router: Router, private routerExtensions: RouterExtensions, private appService: AppService) {
    this._activatedUrl = '/home';
    this._sideDrawerTransition = new SlideInOnTopTransition();

    this.router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);

    this.appService.getMenu().subscribe((data: IMenu[]) => {
      this.fillerNav = data;
    });
  }

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }

  isComponentSelected(url: string): boolean {
    url = '/' + url;
    return this._activatedUrl === url;
  }

  onNavItemTap(navItemRoute: string): void {
    navItemRoute = '/' + navItemRoute;
    this.routerExtensions.navigate([navItemRoute], {
      transition: {
        name: 'fade'
      }
    });

    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.closeDrawer();
  }
}
