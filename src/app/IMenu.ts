interface IMenu {
  displayName: string;
  routeName: string;
}

export {
  IMenu
};
