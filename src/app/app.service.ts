import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {
  private readonly menuUrl = 'http://192.168.1.229:5000/menu';
  constructor(private http: HttpClient) {
  }

  getMenu() {
    return this.http.get(this.menuUrl);
  }
}
